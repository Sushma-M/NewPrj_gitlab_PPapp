/*Copyright (c) 2015-2016 imaginea-com All Rights Reserved.
 This software is the confidential and proprietary information of imaginea-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with imaginea-com*/
package com.passportapp.passportapp;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ApplicantDetails generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`ApplicantDetails`")
public class ApplicantDetails implements Serializable {

    private Integer id;
    private String applicantGivenName;
    private String applicantSurName;
    private String applicationType;
    private String applyingfor;
    private Date dob;
    private String gender;
    private String passportBookeltsize;
    private String reissueReason;
    private Boolean isDraft;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`ApplicantGivenName`", nullable = true, length = 55)
    public String getApplicantGivenName() {
        return this.applicantGivenName;
    }

    public void setApplicantGivenName(String applicantGivenName) {
        this.applicantGivenName = applicantGivenName;
    }

    @Column(name = "`ApplicantSurName`", nullable = true, length = 45)
    public String getApplicantSurName() {
        return this.applicantSurName;
    }

    public void setApplicantSurName(String applicantSurName) {
        this.applicantSurName = applicantSurName;
    }

    @Column(name = "`ApplicationType`", nullable = true, length = 255)
    public String getApplicationType() {
        return this.applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    @Column(name = "`Applyingfor`", nullable = true, length = 255)
    public String getApplyingfor() {
        return this.applyingfor;
    }

    public void setApplyingfor(String applyingfor) {
        this.applyingfor = applyingfor;
    }

    @Column(name = "`DOB`", nullable = true)
    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Column(name = "`Gender`", nullable = true, length = 20)
    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "`PassportBookeltsize`", nullable = true, length = 255)
    public String getPassportBookeltsize() {
        return this.passportBookeltsize;
    }

    public void setPassportBookeltsize(String passportBookeltsize) {
        this.passportBookeltsize = passportBookeltsize;
    }

    @Column(name = "`ReissueReason`", nullable = true, length = 80)
    public String getReissueReason() {
        return this.reissueReason;
    }

    public void setReissueReason(String reissueReason) {
        this.reissueReason = reissueReason;
    }

    @Column(name = "`IsDraft`", nullable = true)
    public Boolean getIsDraft() {
        return this.isDraft;
    }

    public void setIsDraft(Boolean isDraft) {
        this.isDraft = isDraft;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApplicantDetails)) return false;
        final ApplicantDetails applicantDetails = (ApplicantDetails) o;
        return Objects.equals(getId(), applicantDetails.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

